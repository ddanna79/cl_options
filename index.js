function Cl_Options (opts) {
  this._opts = {};
  this._shortcuts = {};
  this._mandatory = new Array();
  for (var i in opts) {
    if (opts[i].command != undefined) {
      this._opts[opts[i].command] = (opts[i].hasValue != undefined) ?  opts[i].hasValue : null;
      if (opts[i].mandatory != undefined && opts[i].mandatory == true) {
        this._mandatory.push(opts[i].command);
      }
      if (opts[i].shortcut != undefined) {
        if (opts[i].shortcut.length == 1) {
          this._shortcuts[opts[i].shortcut] = opts[i].command;
        } else {
          throw new Error("The shortcut " + opts[i].shortcut + " is more than one letter and this is not allowed ");
        }
      }
    } else {
      throw new Error("option " + opts[i].command + " is not a valid name");
    }
  }
}

Cl_Options.prototype.getOptions = function() {
  var params = process.argv.slice(2,process.argv.length);
  var result = {};
  var key = false;
  for (var i in params) {
    //Option Key
    if (params[i][0] == "-") {
      //Full key
      if (params[i][1] == "-") {
        key = params[i].substr(2,params[i].length-2);
        result[key] = null;
      }
      //Shortcut
      else {
        for (var j= 1; j <  params[i].length; j++) {
          if (this._shortcuts[params[i][j]] != undefined) {
            result[this._shortcuts[params[i][j]]] = null;
          } else {
            throw new Error("Unexisting option " + params[i][j]);
          }
        }
        //We completed the loop so we know the key exists
        key = this._shortcuts[ params[i][j-1] ];
      }

    } else {
      //Option value
      if (key !== false) {
        result[key] = params[i];
        key = false;
      } else {
        throw new Error("Options must start with -- if full options, - if shortcuts");
      }
    }
  }

  for (var i in result) {
    if ( (this._opts[i] == true && result[i] == null) || (!this._opts[i] && result[i] != null) ) {
      throw new Error("Invalid option/value supplied " + i + "/" + result[i]);
    } else {
      if (!this._opts[i]) {
        result[i] = true;
      }
    }
  }

  this._checkMandatory(result);

  return result;
}

Cl_Options.prototype._checkMandatory = function(result) {
  for (var i in this._mandatory) {
    if (Object.keys(result).indexOf(this._mandatory[i]) == -1) {
      throw new Error("The option " +  this._mandatory[i] + " is mandatory");
    }
  }

  return true;
}

module.exports = Cl_Options;
