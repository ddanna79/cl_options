var ClO = require('./index');

var opts = [
  {
    command : "name",
    shortcut : "n",
    hasValue : true
  },
  {
    command : "update",
    shortcut : "u",
    mandatory : false
  }
]

var clOpts = new ClO(opts);
var parsed = clOpts.getOptions();
console.log(parsed);
